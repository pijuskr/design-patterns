package com.dp.structural.decorator;

public abstract class CameraDecorator implements ShutterClick {
    protected ShutterClick decoratedCamera;

    public CameraDecorator(ShutterClick shutterClickCamera) {
        this.decoratedCamera = shutterClickCamera;
    }
    public void captureImage() {
        this.decoratedCamera.captureImage();
    }
}
