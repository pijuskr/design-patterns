package com.dp.structural.decorator;

public interface ShutterClick {
    void captureImage();
}
