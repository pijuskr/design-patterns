package com.dp.structural.decorator;

public class CameraWithFrame extends CameraDecorator {
    public CameraWithFrame(ShutterClick shutterClickCamera) {
        super(shutterClickCamera);
    }

    @Override
    public void captureImage() {
        this.addNewFrame();
        this.decoratedCamera.captureImage();
    }

    public void addNewFrame() {
        System.out.println(">> Frame added: ");
    }

}
