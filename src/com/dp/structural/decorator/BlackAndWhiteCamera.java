package com.dp.structural.decorator;

public class BlackAndWhiteCamera implements ShutterClick {
    public BlackAndWhiteCamera() {

    }
    public void captureImage() {
        System.out.println("Camera: B & W | Shutter Click");
    }
}
