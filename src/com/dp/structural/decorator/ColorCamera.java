package com.dp.structural.decorator;

public class ColorCamera implements ShutterClick {
    public ColorCamera() {

    }
    public void captureImage() {
        System.out.println("Camera: Color | Shutter Click");
    }
}
