package com.dp.structural.decorator;

public class CanonColorCamera implements ShutterClick {
    public CanonColorCamera() {

    }
    public void captureImage() {
        System.out.println("Camera: Canon-Color | Shutter Click");
    }
}
