package com.dp.structural.decorator;

public class DecoratorPatternMain {
    public static void main(String[] args) {
        BlackAndWhiteCamera bwCamera = new BlackAndWhiteCamera();
        ColorCamera colorCamera = new ColorCamera();

        // balck & white camera with virtual form.
        CameraWithFrame cameraWithFrame = new CameraWithFrame(bwCamera);
        cameraWithFrame.captureImage();

        CameraWithFrame colorFrame = new CameraWithFrame(colorCamera);
        colorFrame.captureImage();

    }
}
