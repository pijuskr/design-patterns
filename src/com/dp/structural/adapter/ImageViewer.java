package com.dp.structural.adapter;

public class ImageViewer implements FileViewer {
    @Override
    public void viewFile(String fileType, String fileName) {
        System.out.println("ImageViewer: viewFile | File: " + fileName + "." + fileType);
    }
}
