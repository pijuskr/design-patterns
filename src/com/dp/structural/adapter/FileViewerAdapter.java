package com.dp.structural.adapter;

import java.util.ArrayList;
import java.util.List;

public class FileViewerAdapter implements FileViewer {
    FileViewer fileViewer;
    private List<String> imageExt = new ArrayList<String>();
    private List<String> excelExt = new ArrayList<String>();

    FileViewerAdapter(String fileExt) {
        this.imageExt.add("jpg");
        this.imageExt.add("jpeg");
        this.imageExt.add("png");
        this.imageExt.add("gif");
        this.imageExt.add("svg");

        this.excelExt.add("xlsx");
        this.excelExt.add("xls");
        this.excelExt.add("csv");

        if(fileExt.equalsIgnoreCase("pdf")) {
            fileViewer = new PdfViewer();
        }

        if(fileExt.equalsIgnoreCase("html") || fileExt.equalsIgnoreCase("htm")) {
            fileViewer = new HtmlViewer();
        }

        if(imageExt.contains(fileExt.toLowerCase())) {
            fileViewer = new ImageViewer();
        }

        if(excelExt.contains(fileExt.toLowerCase())) {
            fileViewer = new ExcelViewer();
        }
    }
    @Override
    public void viewFile(String fileType, String fileName) {
        fileViewer.viewFile(fileType, fileName);
        // System.out.println("HtmlViewer: viewFile | File: " + fileName + "." + fileType);
    }
}
