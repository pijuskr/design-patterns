package com.dp.structural.adapter;

public class AdapterPatternMain {
    public static void main(String[] args) {
        FileAdapter fileAdapter = new FileAdapter();
        fileAdapter.viewFile("pdf", "Adapter design pattern 1");
        fileAdapter.viewFile("xls", "Adapter design pattern 2");
        fileAdapter.viewFile("png", "Adapter design pattern 3");
        fileAdapter.viewFile("html", "Adapter design pattern 4");

        fileAdapter.editFile("txt", "Adapter design pattern 5");
        fileAdapter.editFile("odt", "Adapter design pattern 6");
        fileAdapter.editFile("csv", "Adapter design pattern 7");

    }
}
