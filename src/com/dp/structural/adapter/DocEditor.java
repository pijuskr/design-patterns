package com.dp.structural.adapter;
import java.util.List;
import java.util.ArrayList;

public class DocEditor implements Editor, FileEditor {
    private List<String> allowedExt = new ArrayList<String>();

    DocEditor() {
        this.allowedExt.add("doc");
        this.allowedExt.add("docx");
        this.allowedExt.add("odt");
    }

    public List<String> getAllowedExtensions() {
        return this.allowedExt;

    }
    @Override
    public void viewFile(String fileType, String fileName) {
        System.out.println("DocEditor: viewFile");
    }

    @Override
    public void editFile(String fileType, String fileName) {
        System.out.println("DocEditor: editFile | File: "+fileName+"."+fileType);
        if(this.isEditable(fileType)) {
            this.startEditing(fileType, fileName);
        } else {
            System.out.println("DocEditor: Can't able open the file in edit mode.");
        }
    }

    @Override
    public boolean isEditable(String fileType) {
        List<String> fileExt = this.getAllowedExtensions();
        if(fileExt.contains(fileType)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void startEditing(String fileType, String fileName) {
        System.out.println("DocEditor: startEditing");
    }
}
