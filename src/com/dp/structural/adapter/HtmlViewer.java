package com.dp.structural.adapter;

public class HtmlViewer implements FileViewer {
    @Override
    public void viewFile(String fileType, String fileName) {
        System.out.println("HtmlViewer: viewFile | File: " + fileName + "." + fileType);
    }
}
