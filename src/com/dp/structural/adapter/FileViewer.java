package com.dp.structural.adapter;

public interface FileViewer {
    public void viewFile(String fileType, String fileName);
}
