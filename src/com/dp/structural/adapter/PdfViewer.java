package com.dp.structural.adapter;

public class PdfViewer implements FileViewer {
    @Override
    public void viewFile(String fileType, String fileName) {
        System.out.println("PdfViewer: viewFile | File: " + fileName + "." + fileType);
    }
}
