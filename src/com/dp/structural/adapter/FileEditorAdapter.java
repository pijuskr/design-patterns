package com.dp.structural.adapter;

import java.util.ArrayList;
import java.util.List;

public class FileEditorAdapter implements FileEditor {
    FileEditor fileEditor;
    private List<String> docEditorExt = new ArrayList<String>();
    private List<String> excelEditorExt = new ArrayList<String>();
    private List<String> textEditorExt = new ArrayList<String>();


    FileEditorAdapter(String fileExt) {
        this.docEditorExt.add("doc");
        this.docEditorExt.add("docx");
        this.docEditorExt.add("odt");

        this.excelEditorExt.add("xlsx");
        this.excelEditorExt.add("xls");
        this.excelEditorExt.add("csv");

        this.textEditorExt.add("txt");
        this.textEditorExt.add("rtf");

        if(docEditorExt.contains(fileExt.toLowerCase())) {
            fileEditor = new DocEditor();
        }

        if(excelEditorExt.contains(fileExt.toLowerCase())) {
            fileEditor = new ExcelEditor();
        }

        if(textEditorExt.contains(fileExt.toLowerCase())) {
            fileEditor = new TextEditor();
        }
    }

    @Override
    public void viewFile(String fileType, String fileName) {
        fileEditor.viewFile(fileType, fileName);
    }

    @Override
    public void editFile(String fileType, String fileName) {
        fileEditor.editFile(fileType, fileName);
    }
}
