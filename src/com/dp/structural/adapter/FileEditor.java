package com.dp.structural.adapter;

public interface FileEditor {
    public void viewFile(String fileType, String fileName);
    public void editFile(String fileType, String fileName);
}
