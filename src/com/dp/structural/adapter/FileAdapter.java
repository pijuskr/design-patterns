package com.dp.structural.adapter;

public class FileAdapter implements FileViewer {
    FileViewer fileViewer;
    FileEditor fileEditor;
    @Override
    public void viewFile(String fileType, String fileName) {
        fileViewer = new FileViewerAdapter(fileType);
        fileViewer.viewFile(fileType, fileName);
    }

    public void editFile(String fileType, String fileName) {
        fileEditor = new FileEditorAdapter(fileType);
        fileEditor.editFile(fileType, fileName);
    }


}
