package com.dp.structural.adapter;

public interface Editor {
    public boolean isEditable(String fileType);
    public void startEditing(String fileType, String fileName);
}
