package com.dp.structural.adapter;

public class ExcelViewer implements FileViewer {
    @Override
    public void viewFile(String fileType, String fileName) {
        System.out.println("ExcelViewer: viewFile | File: " + fileName + "." + fileType);
    }
}
