package com.dp.structural.adapter;

import java.util.ArrayList;
import java.util.List;

public class TextEditor implements Editor, FileEditor {
    private List<String> allowedExt = new ArrayList<String>();

    TextEditor() {
        this.allowedExt.add("txt");
        this.allowedExt.add("rtf");
    }

    public List<String> getAllowedExtensions() {
        return this.allowedExt;

    }
    @Override
    public void viewFile(String fileType, String fileName) {
        System.out.println("TextEditor: viewFile");
    }

    @Override
    public void editFile(String fileType, String fileName) {
        System.out.println("TextEditor: editFile | File: "+fileName+"."+fileType);
        if(this.isEditable(fileType)) {
            this.startEditing(fileType, fileName);
        } else {
            System.out.println("TextEditor: Can't able open the file in edit mode.");
        }
    }

    @Override
    public boolean isEditable(String fileType) {
        List<String> fileExt = this.getAllowedExtensions();
        if(fileExt.contains(fileType)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void startEditing(String fileType, String fileName) {
        System.out.println("TextEditor: startEditing");
    }
}
