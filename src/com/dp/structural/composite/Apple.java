package com.dp.structural.composite;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Apple implements Product {
    private String name;
    private int quantity;
    private double totalPrice;
    List<Product> products = new ArrayList<Product>();

    Apple(String name, int quantity, double totalPrice) {
        this.name = name;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
    }
    public void addProduct(Product product) {
        this.products.add(product);
    }
    public void removeProduct(Product product) {
        this.products.remove(product);
    }

    public List<Product> getProducts() {
        return this.products;
    }

    public String getProductName() {
        return this.name;
    }

    public int getProductQuantity() {
        return this.quantity;
    }

    public double getTotalPrice() {
        return this.totalPrice;
    }

    public void showDetails() {
        System.out.println("-------------------------");
        System.out.println("Name = " + this.getProductName());
        System.out.println("Quantity = " + this.getProductQuantity());
        System.out.println("Total Price = " + this.getTotalPrice());
        System.out.println("-------------------------");

        Iterator<Product> productIterator = this.products.iterator();
        while(productIterator.hasNext()){
            Product product = productIterator.next();
            product.showDetails();
        }
    }
}
