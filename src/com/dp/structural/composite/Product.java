package com.dp.structural.composite;

import java.util.List;

public interface Product {

    public void addProduct(Product product);
    public void removeProduct(Product product);
    public List<Product> getProducts();
    public String getProductName();
    public int getProductQuantity();
    public double getTotalPrice();
    public void showDetails();
}
