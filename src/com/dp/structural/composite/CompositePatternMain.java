package com.dp.structural.composite;

public class CompositePatternMain {
    public static void main(String[] args) {
        Apple apple = new Apple("Adanac  apple", 4, 3.99);
        Orange orance = new Orange("Mandarin orange", 2, 5.99);
        Fruits fruits = new Fruits("Fruits", 6, 9.98 );
        fruits.addProduct(apple);
        fruits.addProduct(orance);

        Grocery grocery = new Grocery("Grocery items: ", 10, 11.00);
        grocery.addProduct(fruits);
        Milk milk = new Milk("Soy", 1, 4.99);
        grocery.addProduct(milk);
        Eggs eggs = new Eggs("Standard Brown Eggs", 18, 4.99);
        grocery.addProduct(eggs);

        Laptop laptop = new Laptop("Macbook Pro 15in", 1, 500.00);

        ShoppingItems items = new ShoppingItems("Black Friday Shopping", 5,  40.94);
        items.addProduct(fruits);
        items.addProduct(laptop);

        items.showDetails();
    }
}

//LearningFeature
//       - Architecture - ProsCons  - Coding
//
//MongoDB -> LearningFeature
//        Architecture
//
//Java -> LearningFeature
//
//BackEndProgramming -> LearningFeature
//    - Java
//    - MongoDB
//
//
//New Application -> LearningFeature
//    1. Back-end Programming
//    2. Front-end Programming
//    3. Dev-ops
//
//
//1. Back-end Programming -> LearningFeature
//    add tools
//
//
//
//ProcessManager (process id, process name, process responsibility, child process)
//
//
//        Dependency ( Dependency[], )
//Application -> Dependency




