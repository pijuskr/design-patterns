package com.dp.creational.singleton;

public class SingletonPattern {
    public static void main(String[] args) {

        //illegal construct
        //Compile Time Error: The constructor SingleObject() is not visible
        //SingleObject object = new SingleObject();

        //Get the only object available
        Student object1 = Student.getInstance();
        Student object2 = Student.getInstance();
        object1.setName("Pijus Kumar Sarker");
        object1.setLevel(5);
        object1.setTotalMarks(88.50);
        object2.setLevel(7);
        object2.printData();
    }
}
// Student Name: Pijus Kumar Sarker
// Student Level: 7
// Student Marks: 88.5