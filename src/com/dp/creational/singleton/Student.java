package com.dp.creational.singleton;

public class Student {
    // create an object of Singleton
    private static Student ourInstance = new Student();

    // make the constructor private so that this class cannot be
    // instantiated
    private Student() {
    }


    public String name;
    public int level;
    public double totalMarks;

    // Get the only object available
    public static Student getInstance() {
        return ourInstance;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setTotalMarks(double totalMarks) {
        this.totalMarks = totalMarks;
    }

    public void printData() {
        System.out.println("Student Name: "+ this.name);
        System.out.println("Student Level: "+ this.level);
        System.out.println("Student Marks: "+ this.totalMarks);
    }

}


