package com.dp.creational.abstractfactory;

public class ConcreteFactoryB extends AbstractFactory {
    public ProductAxBase createProductA(int productNumber){
        return null;
    }

    public ProductBxBase createProductB(int productNumber){
        switch (productNumber) {
            case 1:
                return new ProductB1( Integer.toString(productNumber));
            case 2:
                return new ProductB2( Integer.toString(productNumber));
            case 3:
                return new ProductB2( Integer.toString(productNumber));
            default:
                return new ProductB1( Integer.toString(1));
        }
    }
}
