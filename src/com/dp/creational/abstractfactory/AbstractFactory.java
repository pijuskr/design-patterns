package com.dp.creational.abstractfactory;

abstract class AbstractFactory {
    abstract ProductAxBase createProductA(int productNumber);
    abstract ProductBxBase createProductB(int productNumber);
}
