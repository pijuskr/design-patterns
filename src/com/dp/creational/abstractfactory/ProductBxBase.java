package com.dp.creational.abstractfactory;

abstract class ProductBxBase {
    public abstract void operationBx1();
    public abstract void operationBx2();
}
