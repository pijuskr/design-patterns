package com.dp.creational.abstractfactory;

public class ProductA2 extends ProductAxBase {

    ProductA2(String arg){
        System.out.println("Product A-"+arg);
    } // Implement the code here

    public void operationAx1() { };
    public void operationAx2() { };
}
