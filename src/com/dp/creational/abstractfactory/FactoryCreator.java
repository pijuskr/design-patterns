package com.dp.creational.abstractfactory;

public class FactoryCreator {
    private static AbstractFactory factory = null;
    static AbstractFactory getFactory(String choice){
        if(choice.equals("a")){
            factory = new ConcreteFactoryA();
        }else if(choice.equals("b")){
            factory = new ConcreteFactoryB();
        }
        return factory;
    }
}
