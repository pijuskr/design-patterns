package com.dp.creational.abstractfactory;

public class ProductB3 extends ProductBxBase {
    ProductB3(String arg){
        System.out.println("Product B-"+arg);
    } // Implement the code here

    public void operationBx1() { };
    public void operationBx2() { };
}
