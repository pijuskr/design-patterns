package com.dp.creational.abstractfactory;

public class AbstractFactoryPatternMain {

    public static void main(String[] args) {
        AbstractFactory factoryA = FactoryCreator.getFactory("a");
        ProductAxBase productA = factoryA.createProductA(1);

        AbstractFactory factoryB = FactoryCreator.getFactory("b");
        ProductBxBase productB = factoryB.createProductB(3);
    }
}
