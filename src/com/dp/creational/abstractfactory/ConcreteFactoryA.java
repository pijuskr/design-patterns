package com.dp.creational.abstractfactory;

public class ConcreteFactoryA extends AbstractFactory {

    public ProductAxBase createProductA(int productNumber){
        switch (productNumber) {
            case 1:
                return new ProductA1( Integer.toString(productNumber));
            case 2:
                return new ProductA2( Integer.toString(productNumber));
            default:
                return new ProductA1( Integer.toString(1));
        }
    }

    public ProductBxBase createProductB(int productNumber){
        return null;
    }

}
