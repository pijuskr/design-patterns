package com.dp.creational.abstractfactory;

public class ProductB2 extends ProductBxBase {
    ProductB2(String arg){
        System.out.println("Product B-"+arg);
    } // Implement the code here

    public void operationBx1() { };
    public void operationBx2() { };
}