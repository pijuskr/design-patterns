package com.dp.creational.abstractfactory;

abstract class ProductAxBase {
    public abstract void operationAx1();
    public abstract void operationAx2();
}
