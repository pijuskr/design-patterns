package com.dp.creational.abstractfactory;

public class ProductA1 extends ProductAxBase {

    ProductA1(String arg){
        System.out.println("Product A-"+arg);
    } // Implement the code here

    public void operationAx1() { };
    public void operationAx2() { };
}
