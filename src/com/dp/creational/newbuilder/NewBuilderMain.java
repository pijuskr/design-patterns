package com.dp.creational.newbuilder;

import java.util.ArrayList;
import java.util.List;

public class NewBuilderMain {
    public static void main(String[] args) {
        ProjectBuilder projectBuilder = new ProjectBuilder();
        Project newProject = projectBuilder.prepareNewProject();
        newProject.showResources();

        Project frontendProject = projectBuilder.prepareFrontendProject();
        frontendProject.showResources();

        Project staticProject = projectBuilder.prepareStaticProject();
        staticProject.showResources();

        Project backendMaintananceProject = projectBuilder.prepareBackendMaintananceProject();
        backendMaintananceProject.showResources();

    }
}
