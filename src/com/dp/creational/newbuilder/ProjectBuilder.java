package com.dp.creational.newbuilder;

public class ProjectBuilder {
    public Project prepareNewProject (){
        System.out.println("------------ New Project ------------");
        Project project = new Project();
        ProductOwner productOwner = new ProductOwner();
        project.addEmployee(productOwner);

        Manager manager = new Manager();
        project.addEmployee(manager);

        TeamLead teamLead = new TeamLead();
        project.addEmployee(teamLead);

        Analyst analyst = new Analyst();
        analyst.setNumberOfResource(2);
        project.addEmployee(analyst);

        SrBackendDeveloper srBackendDeveloper = new SrBackendDeveloper();
        srBackendDeveloper.setNumberOfResource(2);
        project.addEmployee(srBackendDeveloper);

        JrBackendDeveloper jrBackendDeveloper = new JrBackendDeveloper();
        jrBackendDeveloper.setNumberOfResource(2);
        project.addEmployee(jrBackendDeveloper);

        SrFrontendDeveloper srFrontendDevelopes = new SrFrontendDeveloper();
        project.addEmployee(srFrontendDevelopes);

        JrFrontendDeveloper jrFrontendDeveloper = new JrFrontendDeveloper();
        jrFrontendDeveloper.setNumberOfResource(2);
        project.addEmployee(jrFrontendDeveloper);

        UxDeveloper uxDeveloper = new UxDeveloper();
        project.addEmployee(uxDeveloper);

        return project;
    }

    public Project prepareBackendMaintananceProject() {
        System.out.println("\n\n------------ Backend Maintanance Project ------------");
        Project project = new Project();
        ProductOwner productOwner = new ProductOwner();
        productOwner.setNumberOfResource(1);
        project.addEmployee(productOwner);

        Manager manager = new Manager();
        project.addEmployee(manager);

        TeamLead teamLead = new TeamLead();
        teamLead.setNumberOfResource(1);
        project.addEmployee(teamLead);

        Analyst analyst = new Analyst();
        analyst.setNumberOfResource(1);
        project.addEmployee(analyst);

        JrBackendDeveloper jrBackendDeveloper = new JrBackendDeveloper();
        jrBackendDeveloper.setNumberOfResource(3);
        project.addEmployee(jrBackendDeveloper);
        return project;
    }

    public Project prepareStaticProject() {
        System.out.println("\n\n------------ Static Project ------------");
        Project project = new Project();

        ProductOwner productOwner = new ProductOwner();
        productOwner.setNumberOfResource(1);
        project.addEmployee(productOwner);

        TeamLead teamLead = new TeamLead();
        teamLead.setNumberOfResource(1);
        project.addEmployee(teamLead);

        SrFrontendDeveloper srFrontendDevelopes = new SrFrontendDeveloper();
        srFrontendDevelopes.setNumberOfResource(1);
        project.addEmployee(srFrontendDevelopes);

        JrFrontendDeveloper jrFrontendDeveloper = new JrFrontendDeveloper();
        jrFrontendDeveloper.setNumberOfResource(1);
        project.addEmployee(jrFrontendDeveloper);

        UxDeveloper uxDeveloper = new UxDeveloper();
        uxDeveloper.setNumberOfResource(2);
        project.addEmployee(uxDeveloper);

        return project;
    }

    public Project prepareFrontendProject() {
        System.out.println("\n\n------------ Frontend Project ------------");
        Project project = new Project();

        ProductOwner productOwner = new ProductOwner();
        productOwner.setNumberOfResource(1);
        project.addEmployee(productOwner);

        Manager manager = new Manager();
        project.addEmployee(manager);

        TeamLead teamLead = new TeamLead();
        teamLead.setNumberOfResource(1);
        project.addEmployee(teamLead);

        SrFrontendDeveloper srFrontendDevelopes = new SrFrontendDeveloper();
        srFrontendDevelopes.setNumberOfResource(1);
        project.addEmployee(srFrontendDevelopes);

        JrFrontendDeveloper jrFrontendDeveloper = new JrFrontendDeveloper();
        jrFrontendDeveloper.setNumberOfResource(2);
        project.addEmployee(jrFrontendDeveloper);

        UxDeveloper uxDeveloper = new UxDeveloper();
        uxDeveloper.setNumberOfResource(2);
        project.addEmployee(uxDeveloper);

        return project;
    }
}
