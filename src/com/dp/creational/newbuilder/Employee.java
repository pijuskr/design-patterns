package com.dp.creational.newbuilder;

public abstract class Employee implements Resource {

    public String resourceType = "";
    public int numberOfResource = 0;

    @Override
    public abstract void setResourceType();

    @Override
    public abstract void setNumberOfResource(int numResources);

    @Override
    public abstract String getResourceType();

    @Override
    public abstract int getNumberOfResource();
}
