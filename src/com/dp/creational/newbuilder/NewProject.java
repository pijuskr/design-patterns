package com.dp.creational.newbuilder;

import java.util.ArrayList;
import java.util.List;

public class NewProject extends Project {
    private List<Employee> resources = new ArrayList<Employee>();

    public void addEmployee(Employee employee) {
        resources.add(employee);
    }

    public void showResources() {
        for (Employee employee : resources) {
            System.out.println("Resource Type : " + employee.getResourceType() + " # Resources: " + employee.getNumberOfResource());
        }
    }
}
