package com.dp.creational.newbuilder;

public class UxDeveloper extends Employee {
    UxDeveloper(){
        this.setResourceType();
        this.setNumberOfResource(1);
    }

    @Override
    public void setResourceType() {
        this.resourceType = "UX Developer";
    }

    @Override
    public void setNumberOfResource(int numResources) {
        this.numberOfResource = numResources;
    }

    @Override
    public String getResourceType() {
        return this.resourceType;
    }

    @Override
    public int getNumberOfResource() {
        return this.numberOfResource;
    }
}
