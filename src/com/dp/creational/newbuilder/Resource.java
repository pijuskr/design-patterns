package com.dp.creational.newbuilder;

public interface Resource {
    public String resourceType = "";
    public int numberOfResource = 0;
    public void setResourceType();
    public void setNumberOfResource(int numResources);
    public String getResourceType();
    public int getNumberOfResource();
}
