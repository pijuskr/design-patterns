package com.dp.creational.builder;

public interface Packing {
    public String pack();
}
