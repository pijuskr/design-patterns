package com.dp.behavioral.observer;

public abstract class Observer {
    protected Bug bug;
    public abstract void update();
}
