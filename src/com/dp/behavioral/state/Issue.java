package com.dp.behavioral.state;

public interface Issue {
    public String issueId = "";
    public void completedCurrentState();
    public void processCurrentState();
}
