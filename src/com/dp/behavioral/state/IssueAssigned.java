package com.dp.behavioral.state;

public class IssueAssigned implements Issue {
    public String issueId = "";

    public IssueAssigned(String issueId) {
        this.issueId = issueId;
    }
    public void completedCurrentState() {
        System.out.println("Completed assigning issue " + this.issueId);
    }

    public void processCurrentState() {
        System.out.println("Processing to assign issue " + this.issueId);
    }

}
