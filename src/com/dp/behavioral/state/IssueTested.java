package com.dp.behavioral.state;

public class IssueTested implements Issue {
    public String issueId = "";

    public IssueTested(String issueId) {
        this.issueId = issueId;
    }

    public void completedCurrentState() {
        System.out.println("Tested Issue " + this.issueId);
    }

    public void processCurrentState() {
        System.out.println("Processing to test issue " + this.issueId);
    }

}
