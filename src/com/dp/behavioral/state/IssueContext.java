package com.dp.behavioral.state;

public class IssueContext {
    private Issue issue;

    public IssueContext() {
        issue = null;
    }

    public void setIssue(Issue issue) {
        this.issue = issue;
    }

    public Issue getIssue() {
        return this.issue;
    }

}
