package com.dp.behavioral.state;

public class IssueClosed implements Issue {
    public String issueId = "";

    public IssueClosed(String issueId) {
        this.issueId = issueId;
    }
    public void completedCurrentState() {
        System.out.println("Closed issue " + this.issueId);
    }

    public void processCurrentState() {
        System.out.println("Processing to close issue " + this.issueId);
    }
}
