package com.dp.behavioral.state;

public class IssueCompleted implements Issue {
    public String issueId = "";

    public IssueCompleted(String issueId) {
        this.issueId = issueId;
    }
    
    public void completedCurrentState() {
        System.out.println("Completed issue " + this.issueId);
    }

    public void processCurrentState() {
        System.out.println("Processing to complete issue " + this.issueId);
    }
}
