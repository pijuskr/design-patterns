package com.dp.behavioral.state;

public class StatePatternMain {
    public static void main(String[] args) {

        IssueContext issueContext = new IssueContext();

        String issueId = "IS-9876";

        IssueCreated newIssue = new IssueCreated(issueId);
        issueContext.setIssue(newIssue);
        issueContext.getIssue().processCurrentState();
        issueContext.getIssue().completedCurrentState();
        System.out.println("- - - - - - - - - - - - - - - - - - ");

        IssueAssigned assignment = new IssueAssigned(issueId);
        issueContext.setIssue(assignment);
        issueContext.getIssue().processCurrentState();
        issueContext.getIssue().completedCurrentState();
        System.out.println("- - - - - - - - - - - - - - - - - - ");

        IssueCompleted completed = new IssueCompleted(issueId);
        issueContext.setIssue(completed);
        issueContext.getIssue().processCurrentState();
        issueContext.getIssue().completedCurrentState();
        System.out.println("- - - - - - - - - - - - - - - - - - ");

        IssueTested testing = new IssueTested(issueId);
        issueContext.setIssue(testing);
        issueContext.getIssue().processCurrentState();
        issueContext.getIssue().completedCurrentState();
        System.out.println("- - - - - - - - - - - - - - - - - - ");

        IssueClosed closed = new IssueClosed(issueId);
        issueContext.setIssue(closed);
        issueContext.getIssue().processCurrentState();
        issueContext.getIssue().completedCurrentState();
        System.out.println("- - - - - - - - - - - - - - - - - - ");

    }
}
