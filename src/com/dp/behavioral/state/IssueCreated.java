package com.dp.behavioral.state;

public class IssueCreated implements Issue {
    public String issueId = "";

    public IssueCreated(String issueId) {
        this.issueId = issueId;
    }

    public void completedCurrentState() {
        System.out.println("Created Issue " + this.issueId);
    }

    public void processCurrentState() {
        System.out.println("Processing to create issue " + this.issueId);
    }
}
